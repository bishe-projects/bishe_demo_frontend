import { configureStore } from '@reduxjs/toolkit';
import storage from 'redux-persist/lib/storage';
import { persistReducer, persistStore } from 'redux-persist';
import userReducer from './user';

const persistConfig = {
  key: "user",
  storage
}

const persistUserReducer = persistReducer(persistConfig, userReducer);

export const store =  configureStore({
  reducer: {
    user: persistUserReducer
  }
})

export const persistor = persistStore(store)