import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    id: 0,
    username: "",
    isLogin: false,
  },
  reducers: {
    loginSuccess: (state, action) => {
      const user = action.payload.user;
      state.id = user.uid;
      state.username = user.username;
      state.isLogin = true;
    },
    logoutSuccess: (state) => {
      state.id = 0;
      state.username = "";
      state.isLogin = false;
    }
  }
})

export const { loginSuccess, logoutSuccess } = userSlice.actions

export default userSlice.reducer