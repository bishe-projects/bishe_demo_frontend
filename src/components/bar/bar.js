const { ArrowBack } = require("@mui/icons-material")
const { AppBar, Toolbar, IconButton, Typography } = require("@mui/material")

const Bar = (props) => {
  const IsBack = () => {
    if(!props.back) {
      return null;
    }
    return (
      <IconButton
        size="large"
        edge="start"
        color="inherit"
        aria-label="back"
        sx={{ mr: 2 }}
        onClick={() => window.history.back()}
      >
        <ArrowBack />
      </IconButton>
    );
  }
  return (
    <AppBar position="static">
      <Toolbar>
        <IsBack />
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          {props.title}
        </Typography>
      </Toolbar>
    </AppBar>
  )
};

export default Bar;