import { Card, CardContent, CardMedia, Typography } from "@mui/material";
import { history } from "cfgs/history";


const CommodityCard = (props) => {
  return (
    <Card onClick={() => history.push(`/commodity/${props.id}`)}>
      <CardContent>
        <CardMedia 
          component="img"
          image={`/commodity/${props.src}.jpeg`}
          sx={{
          }}
        />
        <Typography variant="subtitle1">
          {props.title}
        </Typography>
        <Typography variant="caption">
          {props.desc}
        </Typography>
        <Typography variant="subtitle1" sx={{marginTop: "10px"}}>
          ￥{props.price}
        </Typography>
      </CardContent>
    </Card>
  )
}

export default CommodityCard;