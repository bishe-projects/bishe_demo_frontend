import { Box, Divider, Skeleton, Stack, Typography } from "@mui/material";
import { grey } from "@mui/material/colors";
import OwnCommentList from "apis/user/ownCommentList";
import Bar from "components/bar/bar";
import { useEffect, useState } from "react";

const CommentItem = (props) => {
  return (
    <Stack 
      direction="row" 
      sx={{
        padding: "12px 0",
      }}
    >
      <Stack direction="column" sx={{flexGrow: 1, justifyContent: "space-around"}}>
        <Typography 
          variant="body1" 
          sx={{
            flex: 1,
            wordBreak: "break-all", 
            marginTop: "10px",
            marginBottom: "10px"
          }}
        >
          {props.comment.content}
        </Typography>
        <Typography variant="caption" sx={{color: grey[700]}}>
          {props.comment.createTime}
        </Typography>
      </Stack>
      <Box 
        component="img" 
        src={`/commodity/${props.commodity.name}.jpeg`} 
        sx={{
          height: '100px',
          width: '100px'
        }}
      />
    </Stack>
  )
}

const MyComment = () => {
  const [loading, setLoading] = useState(true);
  const [commentList, setCommentList] = useState([]);
  useEffect(() => {
    OwnCommentList()
    .then(res => {
      setCommentList(res.commentList);
    })
    .catch(err => {})
    .finally(() => {
      setLoading(false);
    })
  }, []);
  return (
    <Box>
      <Bar title="我的评价" back />
      {
        loading ?
        <Stack spacing={1} sx={{margin: "10px 10px 0 10px"}}>
          <Skeleton variant="rounded" height={100} />
          <Skeleton variant="rounded" height={100} />
          <Skeleton variant="rounded" height={100} />
        </Stack>
        :
        <Stack
          direction="column"
          divider={<Divider />}
          sx={{padding: '0 15px 50px 15px'}}
        >
          {commentList.map(item => (
            <CommentItem 
              comment={item.comment}
              commodity={item.commodity}
            />
          ))}
        </Stack>
      }
    </Box>
  )
}

export default MyComment;