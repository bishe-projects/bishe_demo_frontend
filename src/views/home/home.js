import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Unstable_Grid2';

import CommodityCard from "components/commodityCard/commodityCard";
import { Box } from '@mui/material';
import Bar from 'components/bar/bar';
import { useEffect, useState } from 'react';
import GetCommodityList from 'apis/commodity/getList';

const Home = () => {
  const Item = styled(Box)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    margin: "5px 0",
    color: theme.palette.text.secondary,
  }));
  const [commodityList, setCommodityList] = useState([]);
  useEffect(() => {
    GetCommodityList()
    .then(res => {
      setCommodityList(res.commodityList);
    })
    .catch(err => {})
  }, []);
  return (
    <Box>
      <Bar title="首页" />
      <Grid container sx={{margin: '0 10px 60px 10px'}} spacing={2}>
        {commodityList.map((commodity) => (
          <Grid xs={6} key={commodity.id}>
            <Item>
              <CommodityCard
                id={commodity.id}
                src={commodity.name} 
                title={commodity.name}
                desc={commodity.desc}
                price={commodity.price}
              />
            </Item>
          </Grid>
        ))}
      </Grid>
    </Box>
  )
}

export default Home;