import { Box, Button, TextField } from "@mui/material";
import { useState } from "react";
import Bar from "components/bar/bar";
import register from "apis/user/register";
import { LoadingButton } from "@mui/lab";
import { history } from 'cfgs/history';
import message from "components/message/message";
import login from "apis/user/login";
import { useDispatch } from "react-redux";
import { loginSuccess } from "stores/user";

const Login = (props) => {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const handleSubmit = async (evt) => {
    evt.preventDefault();
    const data = new FormData(evt.currentTarget);
    setLoading(true);
    const result = await login({
      username: data.get("username"),
      password: data.get("password"),
    })
    .then(res => {
      message.success({
        content: "登录成功",
        duration: 1500,
      });
      dispatch(loginSuccess(res));
      history.push("/home");
    })
    setLoading(false);
  }

  return (
    <Box 
      component="form" 
      onSubmit={handleSubmit} 
      sx={{
        marginTop: '100px',
        width: '300px'
      }}
    >
      <TextField
        id="username"
        label="用户名"
        name="username"
        variant="standard" 
        fullWidth
        required
      />
      <TextField
        id="password"
        label="密码"
        name="password"
        type="password"
        variant="standard"
        fullWidth
        required
      />
      <LoadingButton 
        type="submit" 
        variant="contained" 
        fullWidth 
        sx={{marginTop: '30px'}}
        loading={loading}
      >
        登录
      </LoadingButton>
      <Button 
        variant="outlined" 
        fullWidth 
        sx={{marginTop: '15px'}}
        onClick={() => props.setStatus("register")}
      >
        注册账号
      </Button>
    </Box>
  )
}

const Register = (props) => {
  const [confirmPasswordErr, setConfirmPasswordErr] = useState(false);
  const [loading, setLoading] = useState(false);
  const handleSubmit = async (evt) => {
    evt.preventDefault();
    const data = new FormData(evt.currentTarget);
    if(data.get("password") !== data.get("confirm_password")) {
      setConfirmPasswordErr(true);
      return;
    }
    setLoading(true);
    const result = await register({
      username: data.get("username"),
      password: data.get("password"),
      confirm_password: data.get("confirm_password"),
    })
    .then(res => {
      message.success({
        content: "注册成功",
        duration: 1500,
      })
      props.setStatus("login")
    })
    .catch(err => {})
    setLoading(false);
  }

  return (
    <Box 
      component="form" 
      onSubmit={handleSubmit} 
      sx={{
        marginTop: '100px',
        width: '300px'
      }}
    >
      <TextField
        id="username"
        label="用户名"
        name="username"
        variant="standard" 
        fullWidth
        required
      />
      <TextField
        id="password"
        label="密码"
        name="password"
        type="password"
        variant="standard"
        fullWidth
        required
      />
      <TextField
        id="confirm_password"
        label="确认密码"
        name="confirm_password"
        type="password"
        variant="standard"
        fullWidth
        required
        error={confirmPasswordErr}
        helperText={confirmPasswordErr ? "两次密码输入不一致" : ""}
      />
      <LoadingButton 
        type="submit" 
        variant="contained" 
        fullWidth 
        sx={{marginTop: '30px'}}
      >
        注册
      </LoadingButton>
      <Button 
        variant="outlined" 
        fullWidth 
        sx={{marginTop: '15px'}}
        onClick={() => props.setStatus("login")}
      >
        登录账号
      </Button>
    </Box>
  )
}

const LoginOrRegister = () => {
  const [status, setStatus] = useState("login");
  const LoginOrRegisterForm = () => {
    if(status === "login") {
      return <Login setStatus={setStatus} />
    } else if(status === "register") {
      return <Register setStatus={setStatus} />
    } else {
      return null;
    }
  }
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
      }}
    >
      <Bar title="登录/注册" back />
      <LoginOrRegisterForm />
    </Box>
  )
};

export default LoginOrRegister;