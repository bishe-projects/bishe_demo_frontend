import { BottomNavigation, BottomNavigationAction, Paper, Box } from '@mui/material';
import { Home, Person, Whatshot } from '@mui/icons-material';
import { useState } from 'react';
import { Link, Outlet } from 'react-router-dom';

const Base = () => {
  const [value, setValue] = useState(window.location.pathname);
  const onChange = (event, newValue) => {
    setValue(newValue);
  }
  const navigations = [
    {
      label: "首页",
      value: "/home",
      icon: <Home />
    },
    {
      label: "热销榜",
      value: "/rank",
      icon: <Whatshot />
    },
    {
      label: "我的",
      value: "/my",
      icon: <Person />
    }
  ]
  return (
    <Box>
      <Outlet />
      <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }} elevation={3}>
        <BottomNavigation
          showLabels
          value={value}
          onChange={onChange}
        >
          {navigations.map(nav => (
            <BottomNavigationAction 
              key={nav.value}
              label={nav.label}
              value={nav.value}
              icon={nav.icon}
              component={Link}
              to={nav.value}
            />
          ))}
        </BottomNavigation>
      </Paper>
    </Box>
  )
}

export default Base;