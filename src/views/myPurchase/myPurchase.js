import { LoadingButton } from "@mui/lab";
import { Box, Button, Divider, Skeleton, Stack, TextField, Typography } from "@mui/material";
import { grey } from "@mui/material/colors";
import CommentOrder from "apis/order/comment";
import GetOrderList from "apis/order/getList";
import { history } from "cfgs/history";
import Bar from "components/bar/bar";
import message from "components/message/message";
import { useEffect, useState } from "react";

const OrderItem = (props) => {
  const [commentOpen, setCommentOpen] = useState(false);
  const CommentBtn = () => {
    if(props.order.status !== 0) {
      return null;
    }
    if(commentOpen) {
      return (
        <Button variant="outlined" size="small" sx={{marginTop: "10px"}} onClick={() => setCommentOpen(false)}>收起</Button>
      )
    }
    return (
      <Button variant="outlined" size="small" sx={{marginTop: "10px"}} onClick={() => setCommentOpen(true)}>评价商品</Button> 
    )
  }
  const CommentField = () => {
    const [commentContent, setCommentContent] = useState("");
    const [loading, setLoading] = useState(false);
    const comment = async () => {
      if(commentContent.trim().length === 0) {
        message.error({content: "输入不能为空", duration: 1000});
        return;
      }
      setLoading(true)
      const result = await CommentOrder({order_id: props.order.id, content: commentContent})
      .then(res => {
        message.success({content: "评论成功", duration: 1000})
        props.updateOrderStatus(props.order.id);
      })
      .catch(err => {})
      setLoading(false);
    }
    if(!commentOpen || props.order.status !== 0) {
      return null;
    }
    return (
      <Stack sx={{marginTop: "10px"}}>
        <TextField
          id={props.order.id.toString()}
          value={commentContent}
          onChange={e => setCommentContent(e.target.value)}
          variant="outlined" 
          size="small" 
          multiline 
          rows={3} 
          placeholder="填写您的评论..." 
        />
        <LoadingButton variant="contained" loading={loading} sx={{marginTop: "10px"}} onClick={comment}>发布评论</LoadingButton>
      </Stack>
    )
  }
  return (
    <Stack sx={{padding: "12px 0"}}>
      <Stack direction="row" onClick={() => history.push(`/commodity/${props.order.commodity.id}`)}>
        <Box 
          component="img" 
          src={`/commodity/${props.order.commodity.name}.jpeg`} 
          sx={{
            height: '100px',
            width: '100px'
          }}
        />
        <Typography variant="subtitle1" sx={{flexGrow: 1}}>
            {props.order.commodity.name}
          </Typography>
        <Stack sx={{alignItems: "end"}}>
          <Typography variant="subtitle1">
            ￥{props.order.commodity.price}
          </Typography>
          <Typography variant="caption" sx={{color: grey[500], flexGrow: 1}}>
            ×{props.order.amount}
          </Typography>
          <Typography sx={{fontWeight: "bold"}}>
            实付￥{props.order.amount * props.order.commodity.price}
          </Typography>
        </Stack>
      </Stack>
      <CommentBtn />
      <CommentField />
    </Stack>
  )
}

const MyPurchase = (props) => {
  const [orderList, setOrderList] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    GetOrderList()
    .then(res => {
      setOrderList(res.orderList);
    })
    .catch(err => {})
    .finally(() => {
      setLoading(false);
    })
  }, []);
  const updateOrderStatus = (orderId) => {
    const orders = [...orderList];
    orders.forEach(order => {
      if(order.id === orderId) {
        order.status = 1;
      }
    });
    setOrderList(orders);
  }
  return (
    <Box>
      <Bar title="我的购买" back />
      {
        loading ? 
        <Stack spacing={1} sx={{margin: "10px 10px 0 10px"}}>
          <Skeleton variant="rounded" height={100} />
          <Skeleton variant="rounded" height={100} />
          <Skeleton variant="rounded" height={100} />
        </Stack>
        :
        <Stack
          divider={<Divider />}
          sx={{padding: '0 30px 50px 15px'}}
        >
          {orderList.map(order => (
            <OrderItem 
              key={order.id}
              order={order}
              updateOrderStatus={updateOrderStatus}
            />
          ))}
        </Stack>
      }
    </Box>
  )
};

export default MyPurchase;