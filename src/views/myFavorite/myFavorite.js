import { Favorite } from "@mui/icons-material";
import { Box, Divider, Skeleton, Stack, Typography } from "@mui/material";
import { pink } from "@mui/material/colors";
import FavoriteCommodity from "apis/commodity/favorite";
import FavoriteCancelCommodity from "apis/commodity/favoriteCancel";
import GetFavoriteList from "apis/commodity/getFavoriteList";
import { history } from "cfgs/history";
import Bar from "components/bar/bar";
import message from "components/message/message";
import { useEffect, useState } from "react";

const FavoriteItem = (props) => {
  const [isFavorite, setIsFavorite] = useState(true);
  const favorite = () => {
    FavoriteCommodity({commodity_id: props.id})
    .then(res => {
      message.success({content: "收藏成功", duration: 1000})
      setIsFavorite(true);
    })
    .catch(err => {})
  }
  const favoriteCancel = () => {
    FavoriteCancelCommodity({commodity_id: props.id})
    .then(res => {
      message.success({content: "取消收藏成功", duration: 1000})
      setIsFavorite(false);
    })
    .catch(err => {})
  }
  return (
    <Stack 
      direction="row" 
      sx={{
        padding: "12px 0",
        alignItems: "center"
      }}
    >
      <Box 
        component="img" 
        src={`/commodity/${props.name}.jpeg`} 
        sx={{
          height: '100px',
          width: '100px'
        }}
        onClick={() => history.push(`/commodity/${props.id}`)}
      />
      <Stack direction="column" sx={{flexGrow: 1}}>
        <Typography variant="subtitle1">
          {props.name}
        </Typography>
        <Typography variant="caption">
          {props.desc}
        </Typography>
        <Typography variant="subtitle1">
          ￥{props.price}
        </Typography>
      </Stack>
      {isFavorite ? <Favorite sx={{color: pink[500]}} onClick={favoriteCancel} /> : <Favorite onClick={favorite}/>}
    </Stack>
  )
}

const MyFavorite = () => {
  const [favoriteList, setFavoriteList] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    GetFavoriteList()
    .then(res => {
      console.log(res.favoriteList);
      setFavoriteList(res.favoriteList);
    })
    .catch(err => {})
    .finally(() => {
      setLoading(false);
    })
  }, []);
    return (
      <Box>
        <Bar title="我的收藏" back />
        {
          loading ? 
          <Stack spacing={1} sx={{margin: "10px 10px 0 10px"}}>
            <Skeleton variant="rounded" height={100} />
            <Skeleton variant="rounded" height={100} />
            <Skeleton variant="rounded" height={100} />
          </Stack>
          :
          <Stack 
            direction="column"
            divider={<Divider />}
            sx={{padding: '0 30px 50px 15px'}}
          >
            {favoriteList.map(commodity => (
              <FavoriteItem
                key={commodity.id}
                id={commodity.id}
                name={commodity.name} 
                desc={commodity.desc}
                price={commodity.price}
              />
            ))}
          </Stack>
        }
      </Box>
    )  
}

export default MyFavorite;