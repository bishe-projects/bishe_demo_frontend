import { ArrowForwardIos } from "@mui/icons-material";
import GetCommodityList from "apis/commodity/getList";
import GetCommodityRankList from "apis/commodity/getRankList";
import Bar from "components/bar/bar";
import { useEffect, useState } from "react";
import { history } from "cfgs/history";

const { Box, Stack, Divider, Typography } = require("@mui/material")

const FIRST_COLOR = "#D4AF37";
const SECOND_COLOR = "#C0C0C0";
const THIRD_COLOR = "#CD7F32";
const OTHER_COLOR = "#808080";

const RankItem = (props) => {
  const RankMark = () => {
    const rankColor = () => {
      var color = OTHER_COLOR;
      switch(props.rank) {
        case 1:
          color = FIRST_COLOR;
          break;
        case 2:
          color = SECOND_COLOR;
          break;
        case 3:
          color = THIRD_COLOR;
          break;
        default:
      }
      return color;
    }
    return (
      <Box 
        sx={{
          bgcolor: rankColor(), 
          width: "20px", 
          height: "20px", 
          color: "white",
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}>
        {props.rank}
      </Box>
    )
  }
  return (
    <Stack 
      direction="row" 
      sx={{
        padding: "12px 0",
        alignItems: "center"
      }}
      onClick={() => history.push(`/commodity/${props.id}`)}
    >
      <RankMark />
      <Box 
        component="img" 
        src={`/commodity/${props.name}.jpeg`} 
        sx={{
          height: '100px',
          width: '100px'
        }}
      />
      <Stack direction="column" sx={{flexGrow: 1}}>
        <Typography variant="body1">
          {props.title}
        </Typography>
        <Typography variant="caption" gutterBottom>
          {props.desc}
        </Typography>
        <Typography variant="subtitle1">
          ￥{props.price}
        </Typography>
      </Stack>
      <ArrowForwardIos />
    </Stack>
  )
}

const Rank = () => {
  const [rankList, setRankList] = useState([]);
  useEffect(() => {
    GetCommodityRankList()
    .then(res => {
      setRankList(res.rankList);
    })
  }, [])
  return (
    <Box>
      <Bar title="热销榜" />
      <Stack 
        direction="column"
        divider={<Divider />}
        sx={{padding: '0 15px 50px 15px'}}
      >
        {rankList.map((commodity, idx) => (
          <RankItem
            key={commodity.id}
            id={commodity.id}
            name={commodity.name} 
            rank={idx+1} 
            title={commodity.name}
            desc={commodity.desc}
            price={commodity.price}
          />
        ))}
      </Stack>
    </Box>
  );
}

export default Rank;