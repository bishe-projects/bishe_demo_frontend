import { Comment, Favorite, Logout, Person, ShoppingBasket } from "@mui/icons-material";
import { Avatar, Box, Card, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Stack, Typography } from "@mui/material";
import { green } from "@mui/material/colors";
import { history } from "cfgs/history";
import Bar from "components/bar/bar";
import { useDispatch, useSelector } from "react-redux";
import { logoutSuccess } from "stores/user";

const NotLogin = () => {
  return (
    <Card sx={{marginTop: '25px'}}>
      <Stack 
        direction="row" 
        spacing={2} 
        sx={{alignItems: "center", padding: "15px 0"}}
        onClick={() => history.push("/login")}
      >
        <Avatar sx={{ bgcolor: green[500], marginLeft: "25px" }}>
          <Person />
        </Avatar>
        <Typography>登录/注册</Typography>
      </Stack>
    </Card>
  )
}

const HasLogin = () => {
  const dispatch = useDispatch();
  const username = useSelector(state => state.user.username);
  const menus = [
    {
      path: "/mypurchase",
      title: "我的购买",
      icon: <ShoppingBasket />,
    },
    {
      path: "/myfavorite",
      title: "我的收藏",
      icon: <Favorite />
    },
    {
      path: "/mycomment",
      title: "我的评论",
      icon: <Comment />
    },
  ]
  const logout = () => {
    localStorage.removeItem("token");
    dispatch(logoutSuccess());
  }
  return (
    <Box>
      <Card sx={{marginTop: "25px"}}>
        <Stack 
          direction="row" 
          spacing={2} 
          sx={{alignItems: "center", padding: "15px 0"}}
        >
          <Avatar sx={{ bgcolor: green[500], marginLeft: "25px" }}>
            <Person />
          </Avatar>
          <Typography>{username}</Typography>
        </Stack>
      </Card>
      <List sx={{marginTop: "10px"}}>
        {menus.map(item => (
          <ListItem key={item.path}>
            <ListItemButton onClick={() => history.push(item.path)}>
              <ListItemIcon>
                {item.icon}
              </ListItemIcon>
              <ListItemText primary={item.title} />
            </ListItemButton>
          </ListItem>
        ))}
        <ListItem>
          <ListItemButton onClick={logout}>
            <ListItemIcon>
              <Logout sx={{color: "#f44336"}} />
            </ListItemIcon>
            <ListItemText 
              disableTypography
              primary={
                <Typography color="#f44336">注销</Typography>
              } 
            />
          </ListItemButton>
        </ListItem>
      </List>
    </Box>
  )
}

const My = () => {
  const isLogin = useSelector(state => state.user.isLogin);
  const Page = () => {
    if(isLogin) {
      return <HasLogin />
    } else {
      return <NotLogin />
    }
  }
  return (
    <Box>
      <Bar title="我的" />
      <Page />
    </Box>
  )
};

export default My;