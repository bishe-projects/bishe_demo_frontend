import { Favorite, Person } from "@mui/icons-material";
import { green, grey, pink } from "@mui/material/colors";
import FavoriteCommodity from "apis/commodity/favorite";
import FavoriteCancelCommodity from "apis/commodity/favoriteCancel";
import GetCommodityById from "apis/commodity/getById";
import HasFavoriteCommodity from "apis/commodity/hasFavorite";
import Bar from "components/bar/bar";
import message from "components/message/message";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { Box, Stack, Typography, Card, CardContent, Button, Divider, Drawer, TextField, Avatar } from "@mui/material";
import { LoadingButton } from "@mui/lab";
import BuyCommodity from "apis/commodity/buy";
import GetCommodityCommentList from "apis/commodity/getCommentList";

const CommodityImg = props => {
  return (
    <Box 
      component="img"
      src={`/commodity/${props.name}.jpeg`}
      sx={{
        width: "100%"
      }}
    />
  );
}

const CommodityInfo = props => {
  const [isFavorite, setIsFavorite] = useState(false);
  const [buyOpen, setBuyOpen] = useState(false);
  const [amount, setAmount] = useState("1");
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    if(props.id === undefined) {
      return;
    }
    HasFavoriteCommodity({commodity_id: props.id})
    .then(res => {
      setIsFavorite(res.has);
    })
    .catch(err => {})
  }, [props.id])
  const FavoriteInfo = () => {
    const favorite = () => {
      if(props.id === undefined) {
        return;
      }
      FavoriteCommodity({commodity_id: props.id})
      .then(res => {
        message.success({content: "收藏成功", duration: 1000})
        setIsFavorite(true);
      })
      .catch(err => {})
    }
    const favoriteCancel = () => {
      if(props.id === undefined) {
        return;
      }
      FavoriteCancelCommodity({commodity_id: props.id})
      .then(res => {
        message.success({content: "取消收藏成功", duration: 1000})
        setIsFavorite(false);
      })
      .catch(err => {})
    }
    if(isFavorite) {
      return (
        <Stack direction="row" sx={{alignItems: "center"}} onClick={favoriteCancel}>
          <Favorite sx={{color: pink[500]}}/>
          <Typography variant="body2" sx={{ marginLeft: "6px" }}>
            取消收藏
          </Typography>
        </Stack>
      )
    } else {
      return (
        <Stack direction="row" sx={{alignItems: "center"}} onClick={favorite}>
          <Favorite />
          <Typography variant="body2" sx={{ marginLeft: "6px" }}>
            收藏
          </Typography>
        </Stack>
      )
    }
  }
  const buy = async () => {
    const n = Number(amount);
    if(Number.isInteger(n) && n > 0 && n <= 100) {
      setLoading(true);
      const result = await BuyCommodity({commodity_id: props.id, amount: n})
      .then(res => {
        message.success({content: "购买成功", duration: 1000});
        setAmount("1");
        setBuyOpen(false);
      })
      .catch(err => {})
      setLoading(false);
    } else {
      message.error({content: "输入数量错误", duration: 1000})
    }
  }
  return (
    <Card sx={{margin: "5px 10px 0 10px", borderRadius: "10px"}}>
      <CardContent>
        <Typography variant="h5" sx={{wordBreak: "break-all"}}>
          {props.name}
        </Typography>
        <Typography variant="body2" sx={{marginTop: "5px"}}>
          {props.desc}
        </Typography>
        <Typography variant="body1" sx={{marginTop: "10px"}}>
          ￥{props.price}
        </Typography>
        <Stack direction="row" sx={{justifyContent: "space-between", alignItems: "center", marginTop: "10px"}}>
          <FavoriteInfo />
          <Button variant="contained" onClick={() => setBuyOpen(true)}>立即购买</Button>
        </Stack>
        <Drawer anchor="bottom" open={buyOpen} onClose={() => setBuyOpen(false)}>
          <Stack direction="column">
            <Stack direction="row" sx={{alignItems: "center"}}>
              <Box 
                component="img"
                src={`/commodity/${props.name}.jpeg`}
                sx={{
                  width: "100px",
                  height: "100px",
                }}
              />
              <Stack direction="column" sx={{marginLeft: "5px"}}>
                <Typography>
                  {props.name}
                </Typography>
                <Typography>
                  ￥{props.price}
                </Typography>
              </Stack>
            </Stack>
            <Stack direction="row" sx={{alignItems: "center", marginLeft: "20px", marginTop: "10px"}}>
              <Typography>数量</Typography>
              <TextField 
                type="number" 
                size="small"
                value={amount} 
                onChange={e => setAmount(e.target.value)}
                sx={{ marginLeft: "15px", width: "100px" }}
              />
            </Stack>
            <LoadingButton 
              variant="contained" 
              loading={loading}
              sx={{margin: "20px"}} 
              onClick={buy}>
                购买
            </LoadingButton>
          </Stack>
        </Drawer>
      </CardContent>
    </Card>
  );
}

const CommentItem = props => {
  return (
    <Stack direction="column" sx={{marginTop: "10px"}}>
      <Stack direction="row">
        <Avatar sx={{ bgcolor: green[500] }}>
          <Person />
        </Avatar>
        <Stack direction="column" sx={{marginLeft: "12px"}}>
          <Typography>
            {props.user.username}
          </Typography>
          <Typography variant="caption" sx={{color: grey[700]}}>
            {props.comment.createTime}
          </Typography>
        </Stack>
      </Stack>
      <Typography sx={{marginTop: "5px"}}>
        {props.comment.content}
      </Typography>
    </Stack>
  )
}

const CommodityComment = props => {
  return (
    <Card sx={{margin: "5px 10px 0 10px", borderRadius: "10px"}}>
      <CardContent>
        <Typography>
          用户评论
        </Typography>
        <Divider sx={{margin: "8px 0"}} />
        <Stack direction="column">
          {props.commentList.map(item => (
            <CommentItem comment={item.comment} user={item.user} />
          ))}
        </Stack>
      </CardContent>
    </Card>
  );
}

const Commodity = props => {
  const params = useParams();
  const [commodity, setCommodity] = useState({});
  const [commentList, setCommentList] = useState([]);
  useEffect(() => {
    const commodityId = params["id"]
    GetCommodityById({commodity_id: parseInt(commodityId)})
    .then(res => {
      setCommodity(res.commodity);
    })
    .catch(err => {})
    GetCommodityCommentList({commodity_id: parseInt(commodityId)})
    .then(res => {
      setCommentList(res.commentList);
    })
    .catch(err => {})
  }, [params["id"]]);
  return (
    <Box sx={{bgcolor: grey[200]}}>
      <Bar title="商品详情" back />
      <CommodityImg name={commodity.name} />
      <CommodityInfo 
        id={commodity.id}
        name={commodity.name}
        desc={commodity.desc}
        price={commodity.price} 
      />
      <CommodityComment commentList={commentList} />
    </Box>
  )
}

export default Commodity;