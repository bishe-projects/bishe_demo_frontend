import axios from 'cfgs/axios';

export default function GetOrderList() {
  return new Promise((resolve, reject) => {
    axios.post("http://127.0.0.1:8087/order/getList")
    .then(res => {
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    })
  });
}