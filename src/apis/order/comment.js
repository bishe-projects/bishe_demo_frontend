import axios from 'cfgs/axios';

export default function CommentOrder(values) {
  return new Promise((resolve, reject) => {
    axios.post("http://127.0.0.1:8087/order/comment", values)
    .then(res => {
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    })
  });
}