import axios from 'cfgs/axios';
import { history } from 'cfgs/history';

export default function login(values) {
  return new Promise((resolve, reject) => {
    axios.post("http://127.0.0.1:8086/user/login", values)
    .then(res => {
      localStorage.setItem("token", res.data.token)
      history.push("/home")
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    })
  });
}