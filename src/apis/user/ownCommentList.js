import axios from 'cfgs/axios';

export default function OwnCommentList(values) {
  return new Promise((resolve, reject) => {
    axios.post("http://127.0.0.1:8086/user/ownCommentList")
    .then(res => {
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    })
  });
}