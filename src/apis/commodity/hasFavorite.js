import axios from 'cfgs/axios';

export default function HasFavoriteCommodity(values) {
  return new Promise((resolve, reject) => {
    axios.post("http://127.0.0.1:8087/commodity/hasFavorite", values)
    .then(res => {
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    })
  });
}