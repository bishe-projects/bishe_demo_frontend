import axios from 'cfgs/axios';

export default function FavoriteCancelCommodity(values) {
  return new Promise((resolve, reject) => {
    axios.post("http://127.0.0.1:8087/commodity/favoriteCancel", values)
    .then(res => {
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    })
  });
}