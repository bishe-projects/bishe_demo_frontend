import axios from 'cfgs/axios';

export default function FavoriteCommodity(values) {
  return new Promise((resolve, reject) => {
    axios.post("http://127.0.0.1:8087/commodity/favorite", values)
    .then(res => {
      resolve(res.data);
    })
    .catch(err => {
      reject(err);
    })
  });
}