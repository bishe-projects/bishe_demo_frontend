import { Routes, Route } from "react-router-dom";
import { CustomRouter, history } from "cfgs/history";

import Base from 'views/base/base';
import Home from 'views/home/home';
import MyFavorite from "views/myFavorite/myFavorite";
import My from "views/my/my";
import LoginOrRegister from "views/login/login";
import Rank from "views/rank/rank";
import MyComment from "views/myComment/myComment";
import Commodity from "views/commodity/commodity";
import MyPurchase from "views/myPurchase/myPurchase";

function App() {
  return (
    <div className="App">
      <CustomRouter history={history}>
        <Routes>
          <Route path="/" element={<Base />}>
            <Route path="/home" element={<Home />} />
            <Route path="/rank" element={<Rank />} />
            <Route path="/my" element={<My />} />
          </Route>
          <Route path="/login" element={<LoginOrRegister />} />
          <Route path="/mypurchase" element={<MyPurchase />} />
          <Route path="/myfavorite" element={<MyFavorite />} />
          <Route path="/mycomment" element={<MyComment />} />
          <Route path="/commodity/:id" element={<Commodity />} />
          {/* <Route path="home" element={<Home />}>
            <Route path="overview" element={<Overview />} />
            <Route path="business" element={<Business />} />
            <Route path="ranklist" element={<Ranklist />} />
            <Route path="comment" element={<Comment />} />
            <Route path="interaction" element={<Interaction />} />
            <Route path="message" element={<Message />} />
          </Route> */}
        </Routes>
      </CustomRouter>
    </div>
  );
}

export default App;
