import axios from 'axios'
// import { message } from 'antd'
import { history } from 'cfgs/history'

const instance = axios.create({
  baseURL: ''
});

let httpCode = {
  //这里我简单列出一些常见的http状态码信息，可以自己去调整配置
  400: '请求参数错误',
  401: '权限不足, 请重新登录',
  403: '您暂无该权限',
  404: '请求资源未找到',
  500: '内部服务器错误',
  501: '服务器不支持该请求中使用的方法',
  502: '网关错误',
  504: '网关超时',
};

instance.interceptors.request.use(
  config => {
    if (localStorage.token) {
      //判断token是否存在
      config.headers.token = localStorage.token; //将token设置成请求头
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  },
);
instance.interceptors.response.use(
  res => {
    if(res.data && res.data.BaseResp && res.data.BaseResp.status < 0) {
      let status = res.data.BaseResp.status;
      let msg = res.data.BaseResp.message;
      let tips = -status in httpCode ? httpCode[-status] : msg
      // message.error(tips);
      if(status === -401) {
        //如果token有问题，跳转到登陆界面
        history.push("/")
      }
      return Promise.reject(new Error(msg));
    }
    return res;
  },
  error => {
    console.log(error)
    if(error.response) {
      let tips =
        error.response.status in httpCode
          ? httpCode[error.response.status]
          : error.response.data.message;
      // 如果错误是403 则显示后端返回的错误
      if (error.response.status === 403) {
        console.log(error.response);
        tips = error.response.data.message;
      }
      // message.error(tips);
      if (error.response.status === 401) {
        //如果token有问题，跳转到登陆界面
        history.push("/")
      }
    } else {
      // message.error("连接超时，请重试");
    }
    return Promise.reject(error);
  },
);

export default instance
